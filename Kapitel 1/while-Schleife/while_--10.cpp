// while_--10.cpp
// Alexander 'Glubschii' Wacks
// 10.03.2015
// v.1.0.0

#include <iostream>

int main()
{
	int sum = 0, i = 10;

	while (i >= 1)
	{
		std::cout << i << std::endl;
		--i;
	}

	return 0;
}