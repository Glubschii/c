// while_range.cpp
// Alexander 'Glubschii' Wacks
// 10.03.2015
// v.1.0.0

#include <iostream>

int main()
{
	int start = 0, end = 0;

	std::cout << "Geben Sie bitte zwei Zahlen ein: " << std::endl;
	std::cin >> start >> end;


		while (end >= start)
		{
			std::cout << end << std::endl;
			--end;
		}

	return 0;
}