// while_50_100.cpp
// Alexander 'Glubschii' Wacks
// 10.03.2015
// v.1.0.0

#include <iostream>

int main()
{
	int sum = 0, i = 50;

	while (i <= 100)
	{
		sum += i;
		++i;
	}

	std::cout << "Die Summe aller Zahlen von 50 bis 100 ist " << sum << std::endl;

	return 0;
}