// while_test.cpp
// Alexander 'Glubschii' Wacks
// 10.03.2015
// v.1.0.0

#include <iostream>

int main()
{
	int sum = 0, i = 1;

	while (i <= 10)
	{
		sum += i;
		++i;
	}

	std::cout << "Die Summe aller Zahlen von 1 bis 10 ist " << sum << std::endl;

	return 0;
}