// int_multiplication.cpp
// Alexander 'Glubschii' Wacks
// 10.03.2015
// v.1.0.0

#include <iostream>

int main()
{
	int v1 = 0, v2 = 0;

	std::cout << "Bitte geben Sie zwei Zahlen fuer die Multiplikation ein: " << std::endl;
	std::cin >> v1 >> v2;

	std::cout << "Das Produkt von " << v1 << " und " << v2 << " ist " << v1 * v2 << std::endl;

	return 0;
}