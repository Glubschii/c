// Hello_World.cpp
// Alexander 'Glubschii' Wacks
// 10.03.2015
// v.1.0.0

#include <iostream>

int main()
{
	std::cout << "Hello World!" << std::endl;

	return 0;
}
