// io_large.cpp
// Alexander 'Glubschii' Wacks
// 10.03.2015
// v.1.0.0

#include <iostream> // Std. IO-Library

int main()
{
	int v1 = 0, v2 = 0;

	std::cout << "Geben Sie zwei Zahlen ein: " << std::endl;	// std::cout - Ausgabe im Fenster | std::endl - Linefeed?
	std::cin >> v1 >> v2;										// std:cin - Eingabe im Fenster

	std::cout << "Die Summe von " << v1;
	std::cout << " and " << v2;
	std::cout << " ist " << v1 + v2 << std::endl;

	return 0;
}